package sm2

import (
	"encoding/asn1"
	"math/big"
)

func MarshalSign2(sign []byte) ([]byte, error) {
	var r, s *big.Int

	r = new(big.Int)
	s = new(big.Int)

	r.SetBytes(sign[0:32])
	s.SetBytes(sign[32:64])

	result, err := asn1.Marshal(sm2Signature{r, s})
	if err != nil {
		return nil, err
	}
	return result, nil
}

func format(bytes []byte) (newbytes []byte) {
	newbytes = make([]byte, 32)
	length := len(bytes)
	switch {
	case length == 32:
		newbytes = bytes
	case length > 32:
		newbytes = bytes[length-32:]
	case length < 32:
		for i := 32 - length; i < 32; i++ {
			newbytes[i] = bytes[i-(32-length)]
		}
	}
	return
}

func Sign2(priv *PrivateKey, userId []byte, in []byte) ([]byte, error) {
	r, s, err := SignToRS(priv, userId, in)
	if err != nil {
		return nil, err
	}

	rbytes := r.Bytes()
	sbytes := s.Bytes()

	rbytes = format(rbytes)
	sbytes = format(sbytes)

	result := make([]byte, 64, 64)

	_, _ = rbytes, sbytes

	for i := 0; i < 32; i++ {
		result[i] = rbytes[i]
		result[i+32] = sbytes[i]
	}

	return result, err
}
